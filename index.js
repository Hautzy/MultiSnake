var express = require('express')
,   app = express()
,   server = require('http').createServer(app)
,   io = require('socket.io').listen(server)
,   generateName = require('sillyname')

var port = process.env.PORT || 8080;
console.log('Websocket server running on port ' + port)
server.listen(port)

app.use(express.static('public'))

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/public/client/index.html')
})

app.get('/removeAllItems', (req, res) => {
  var cnt_items = items.length
  items = []
  res.status(200).send({
    deletedItems: cnt_items
  })
})

app.get('/viewStats', (req, res) => {
  res.status(200).send(
    JSON.stringify({
      players: players
    })
  )
})

var MAX_PLAYER_ID = 0

var MAX_WIDTH = 1000/8
var MAX_HEIGHT = 600/8

var players = {}
var items = []
var clientPlayers = {}

var randomColor = () => { return '#'+(Math.random()*0xFFFFFF<<0).toString(16) }

var createItemInterval = 5000
var createRandomItem = () => {
  if(items.length <= 20) {
    itemTypes = [
      {name: "Apple", exp: 2, color: "#f44242", effect: addOneTailToSnake},
      {name: "Banana", exp: 4, color: "#c5f442", effect: addOneTailToSnake},
    ]

    var randomNr = Math.floor((Math.random() * 2))
    var x = Math.floor((Math.random() * MAX_WIDTH))
    var y = Math.floor((Math.random() * MAX_HEIGHT))

    newItem = {
      status: 0,
      x: x,
      y: y,
      itemType: itemTypes[randomNr]
    }
    
    items.push(newItem)
    //console.log("generated item!" + JSON.stringify(newItem));
  }
}
setInterval(createRandomItem, createItemInterval)

var addOneTailToSnake = (snake) => {
    var tail = snake.body[snake.body.length - 1]
    var newTail = {
      x: tail.x - 1,
      y: tail.y
    }
    snake.body.push(newTail)
}

var getCurrentSeconds = () => {
  var dt = new Date();
  var secs = dt.getSeconds() + (60 * dt.getMinutes()) + (60 * 60 * dt.getHours());
  return secs
}

// check if snake is offline
var garbageHautzyInterval = 1000
//Keeps track of all ACTIVE clients
var garbageHautzy = () => {
  //console.log('garbage collect')
  for(var key in clientPlayers){
    var timestamp = getCurrentSeconds() - clientPlayers[key].timestamp
    if(timestamp > 1) {
      delete players[key]
      delete clientPlayers[key]
    }
  }
  var cleanedItems = []
  for(var i = 0; i < items.length; i++){
    if(items[i].status === 0)
      cleanedItems.push(items[i])
  }
  items = cleanedItems

  for(var key in players) {
    if(players[key].snake.spawnProtection > 0)
      players[key].snake.spawnProtection--
  }
}
setInterval(garbageHautzy, garbageHautzyInterval)

io.sockets.on('connection', (socket) => {
  let x = Math.floor((Math.random() * (MAX_WIDTH - 30))) + 5
  let y = Math.floor((Math.random() * (MAX_HEIGHT - 10))) + 5

  let newPlayer = {
    id: MAX_PLAYER_ID,
    timestamp: getCurrentSeconds(),
    snake: {
      name: generateName().split(' ')[1],
      status: "NON",
      bodyColor: randomColor(),
      borderColor: randomColor(),
      spawnProtection: 5,
      exp: 0.0,
      body: [
        {x:x, y:y},
        {x:x-1, y:y},
        {x:x-2, y:y},
      ]
    }
  }
  players[MAX_PLAYER_ID] = newPlayer

  socket.emit('chat', { time: new Date(), text: 'You connected to the server!', playerId: MAX_PLAYER_ID , type: "info"})
  socket.emit('snacke_draw', { time: new Date(), players: players, items: items })
  MAX_PLAYER_ID++

  //Main game loop - Collissions
  var mainGameLoopInterval = 100
  var mainGameLoop = () => {

    //Collission with other snake and item
    for(var cur_key in players) {

      var currentSnake = players[cur_key].snake
      if(currentSnake.status === "NON" && currentSnake.spawnProtection <= 0){
        var currentHead = currentSnake.body[0]
        //Collision with other snake
        for(var sec_key in players) {

          var secondarySnake = players[sec_key].snake
          if(secondarySnake.spawnProtection > 0)
            continue
          for(var i = 0; i < secondarySnake.body.length; i++) {
            if(sec_key === cur_key && i === 0)
              continue;
            var block = secondarySnake.body[i]
            if(block.x === currentHead.x && block.y === currentHead.y) {
              currentSnake.status = "COL_WITH_SNAKE"
              currentSnake.bodyColor = "#f48342"
              if(sec_key === cur_key)
                io.sockets.emit('chat', {time: new Date(), text: currentSnake.name + " crashed in his tail!", type: "danger"})
              else {
                io.sockets.emit('chat', {time: new Date(), text: currentSnake.name + " was killed by " + secondarySnake.name + "!", type: "danger" })
                secondarySnake.exp += 10.0
              }
            }
          }
        }

        //Collision with items
        for(var i = 0; i < items.length; i++){
          if(items[i].status !== 0)
            continue
          var item = items[i]
          if(item.x === currentHead.x && item.y === currentHead.y){
            currentSnake.exp += item.itemType.exp

            item.itemType.effect(currentSnake)
            item.status = 1
          }
        }
      }
    }

    socket.emit('snacke_draw', { time: new Date(), players: players, items: items })
  }
  setInterval(mainGameLoop, mainGameLoopInterval)

  //Tracks for snake movement
  socket.on('snake_action', (data) => {
    var clientPlayer = data.clientPlayer
    clientPlayer.timestamp = getCurrentSeconds()
    clientPlayers[clientPlayer.id] = clientPlayer

    if(players.length === 0)
      return
    if(!clientPlayer.id in players)
      return
    try{
      if(players[clientPlayer.id] === 'undefined' || players[clientPlayer.id].snake === 'undefined')
        return
    }catch(e){
      return
    }
    if(players[clientPlayer.id].snake.status !== "NON")
      return
    var body = players[clientPlayer.id].snake.body

    var head = {
      x: body[0].x + clientPlayer.velocity.x,
      y: body[0].y + clientPlayer.velocity.y
    }
    if(head.x < 0 || head.y < 0 || head.x >= MAX_WIDTH || head.y >= MAX_HEIGHT) {
      players[clientPlayer.id].snake.status = "OUT_OF_MAP"
      players[clientPlayer.id].snake.bodyColor = "#f48342"
      io.sockets.emit('chat', {time: new Date(), text: players[clientPlayer.id].snake.name + " crashed into the wall!", type: "danger" })
      return
    }
    body.splice(0, 0, head)
    body.pop()
  })

  //Client disconnected check
  socket.on('disconnect', (data) => {
    //Delete snake if dissconnect
    console.log("A client disconnected!!!")
  })
})
