$(document).ready(function () {
  var MAX_GAME_WIDTH = 1000
  var MAX_GAME_HEIGHT = 600
  var MAX_DRAW_WIDTH = 8
  var MAX_DRAW_HEIGHT = 8
  var myCanvas = $('#myCanvas')
  var socket = io.connect()

  var getCurrentSeconds = () => {
    var dt = new Date();
    var secs = dt.getSeconds() + (60 * dt.getMinutes()) + (60 * 60 * dt.getHours());
    return secs
  }

  var getCurrentMilliSeconds = () => {
    var dt = new Date();
    return dt.getTime()
  }

  var players = {}
  var items = []
  var clientPlayer = {
    id: -1,
    timestamp: 0,
    velocity: {
      x: 1,
      y: 0
    }
  }

  var pre_key = 100
  var pre_key_time = getCurrentMilliSeconds()

  //Get key input
  $(document).keypress((e) => {
    /* Act on the event
      w -> 119
      d -> 100
      s -> 115
      a -> 97
    */
    var newX = 0
    var newY = 0
    var toReturn = false
    var curMilliSec = getCurrentMilliSeconds()
    if (curMilliSec - pre_key_time < 80)
      toReturn = true
    //console.log(curMilliSec - pre_key_time)
    //console.log(e.which)
    //console.log('ms_div: ' + (curMilliSec-pre_key_time))
    if (e.which == 119)
      newY = -1
    else if (e.which == 115)
      newY = 1
    else if (e.which == 100)
      newX = 1
    else if (e.which == 97)
      newX = -1

    pre_key = e.which
    pre_key_time = curMilliSec
    if (toReturn)
      return

    if (newX === -clientPlayer.velocity.x)
      return
    else if (newY === -clientPlayer.velocity.y)
      return
    else {
      clientPlayer.velocity.x = newX
      clientPlayer.velocity.y = newY
    }
  });

  //Get chat input
  socket.on('chat', (data) => {
    if (typeof data.playerId !== 'undefined')
      clientPlayer.id = data.playerId

    var time = new Date(data.time)
    if (data.type === "danger") {
      $('#content').append(
        $('<div class="alert alert-danger"></div>').append(
          // Uhrzeit
          $('<span>').text('[' +
            (time.getHours() < 10 ? '0' + time.getHours() : time.getHours()) +
            ':' +
            (time.getMinutes() < 10 ? '0' + time.getMinutes() : time.getMinutes()) +
            '] '
          ),
          // Text
          $('<span>').text(data.text))
      )
    } else {
      $('#content').append(
        $('<div class="alert alert-success"></div>').append(
          // Uhrzeit
          $('<span>').text('[' +
            (time.getHours() < 10 ? '0' + time.getHours() : time.getHours()) +
            ':' +
            (time.getMinutes() < 10 ? '0' + time.getMinutes() : time.getMinutes()) +
            '] '
          ),
          // Text
          $('<span>').text(data.text))
      )
    }
    var content = $('#content')
    content.scrollTop = content.scrollHeight
  })

  //
  var gameOverInterval = 1000
  var seconds = 0
  var gameOverIntervalFunc;
  var gameOverRefresh = () => {
    switch (seconds) {
      case 1:
        myCanvas.drawRect({
          fillStyle: 'white',
          strokeStyle: 'blue',
          strokeWidth: 0,
          x: 0,
          y: 0,
          fromCenter: false,
          width: MAX_GAME_WIDTH,
          height: MAX_GAME_HEIGHT
        })
        myCanvas.drawText({
          fillStyle: '#f44242',
          strokeStyle: '#000000',
          strokeWidth: 1,
          x: 500,
          y: 300,
          fontSize: 68,
          fontFamily: 'Verdana, sans-serif',
          text: 'Refreshing in...'
        });
        break
      case 2:
        myCanvas.drawRect({
          fillStyle: 'white',
          strokeStyle: 'blue',
          strokeWidth: 0,
          x: 0,
          y: 0,
          fromCenter: false,
          width: MAX_GAME_WIDTH,
          height: MAX_GAME_HEIGHT
        })
        myCanvas.drawText({
          fillStyle: '#f44242',
          strokeStyle: '#000000',
          strokeWidth: 1,
          x: 500,
          y: 300,
          fontSize: 68,
          fontFamily: 'Verdana, sans-serif',
          text: '3'
        });
        break
      case 3:
        myCanvas.drawRect({
          fillStyle: 'white',
          strokeStyle: 'blue',
          strokeWidth: 0,
          x: 0,
          y: 0,
          fromCenter: false,
          width: MAX_GAME_WIDTH,
          height: MAX_GAME_HEIGHT
        })
        myCanvas.drawText({
          fillStyle: '#f44242',
          strokeStyle: '#000000',
          strokeWidth: 1,
          x: 500,
          y: 300,
          fontSize: 68,
          fontFamily: 'Verdana, sans-serif',
          text: '2'
        });
        break
      case 4:
        myCanvas.drawRect({
          fillStyle: 'white',
          strokeStyle: 'blue',
          strokeWidth: 0,
          x: 0,
          y: 0,
          fromCenter: false,
          width: MAX_GAME_WIDTH,
          height: MAX_GAME_HEIGHT
        })
        myCanvas.drawText({
          fillStyle: '#f44242',
          strokeStyle: '#000000',
          strokeWidth: 1,
          x: 500,
          y: 300,
          fontSize: 68,
          fontFamily: 'Verdana, sans-serif',
          text: '1'
        });
        break
      case 5:
        clearInterval(gameOverIntervalFunc)
        location.reload()
        break
    }
    seconds++
  }
  //

  var interval_snake_render = 50
  var stop_main_render = false
  var renderSnakes = () => {
    if (stop_main_render)
      return

    myCanvas.drawRect({
      fillStyle: 'white',
      strokeStyle: 'blue',
      strokeWidth: 0,
      x: 0,
      y: 0,
      fromCenter: false,
      width: MAX_GAME_WIDTH,
      height: MAX_GAME_HEIGHT
    })

    var cnt = 0;
    for (var player in players)
      cnt++
      if (typeof players[clientPlayer.id] !== 'undefined' && typeof players[clientPlayer.id].snake !== 'undefined') {
        if (players[clientPlayer.id].snake.spawnProtection > 0)
          $('#protection_of_player').html("<span class='label label-primary'>" + players[clientPlayer.id].snake.spawnProtection + "</span> Spawn Protection")
        else
          $('#protection_of_player').html("")
        $('#length_of_player').text(players[clientPlayer.id].snake.body.length)
        $('#color_of_player').css('background-color', players[clientPlayer.id].snake.bodyColor)
        $('#color_of_player').css('border', "4px solid " + players[clientPlayer.id].snake.borderColor)
        $('#name_of_player').text(players[clientPlayer.id].snake.name)
        $('#cnt_of_players').html("<span class='label label-danger'>" + cnt + "</span> Spieler online")
        $('#exp_of_player').text(players[clientPlayer.id].snake.exp)
      }

    //Draw items
    for (var i = 0; i < items.length; i++) {
      if (items[i].status !== 0)
        continue
      myCanvas.drawRect({
        fillStyle: items[i].itemType.color,
        strokeStyle: "black",
        strokeWidth: 1,
        x: items[i].x * MAX_DRAW_WIDTH,
        y: items[i].y * MAX_DRAW_HEIGHT,
        fromCenter: false,
        width: MAX_DRAW_WIDTH,
        height: MAX_DRAW_HEIGHT
      })
    }

    //Draw snake body
    for (var key in players) {

      let currentSnake = players[key].snake
      for (let i = 0; i < currentSnake.body.length; i++) {
        if (i === 0) {
          myCanvas.drawRect({
            fillStyle: currentSnake.borderColor,
            strokeStyle: currentSnake.bodyColor,
            strokeWidth: 1,
            x: currentSnake.body[i].x * MAX_DRAW_WIDTH,
            y: currentSnake.body[i].y * MAX_DRAW_HEIGHT,
            fromCenter: false,
            width: MAX_DRAW_WIDTH,
            height: MAX_DRAW_HEIGHT
          })
        } else {
          myCanvas.drawRect({
            fillStyle: currentSnake.bodyColor,
            strokeStyle: currentSnake.borderColor,
            strokeWidth: 1,
            x: currentSnake.body[i].x * MAX_DRAW_WIDTH,
            y: currentSnake.body[i].y * MAX_DRAW_HEIGHT,
            fromCenter: false,
            width: MAX_DRAW_WIDTH,
            height: MAX_DRAW_HEIGHT
          })
        }
      }
    }

    if (typeof players[clientPlayer.id] !== 'undefined' && typeof players[clientPlayer.id].snake !== 'undefined' && players[clientPlayer.id].snake.status !== "NON") {
      stop_main_render = true

      myCanvas.drawText({
        fillStyle: '#f44242',
        strokeStyle: '#000000',
        strokeWidth: 1,
        x: 500,
        y: 300,
        fontSize: 68,
        fontFamily: 'Verdana, sans-serif',
        text: 'GAME OVER!'
      });

      gameOverIntervalFunc = setInterval(gameOverRefresh, gameOverInterval)
    }
  }

  setInterval(renderSnakes, interval_snake_render)

  //Define main game loop
  var interval_snake_action = 100
  var mainloop = () => {
    //Send to server

    //clientPlayer.timestamp = getCurrentSeconds()
    socket.emit('snake_action', {
      clientPlayer: clientPlayer
    })
  }

  setInterval(mainloop, interval_snake_action)

  socket.on('snacke_draw', (data) => { //Get draw data from server
    players = data.players
    items = data.items
  })

  $(window).on('beforeunload', function () {
    return 'Are you sure you want to leave?';
  });

  $(window).on('unload', function () {
    socket.emit('disconnect', {
      text: "I am leaving"
    })
  });

});